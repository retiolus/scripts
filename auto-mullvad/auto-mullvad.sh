#!/bin/bash

# Function to print errors to stderr with timestamp
err() {
  echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $*" >&2
}

# Whitelist of trusted networks (replace with your own UUIDs)
whitelist=(
    "UUID HERE"
    "ANOTHER UUID HERE"
)

is_mullvad_connected() {
    mullvad status | grep -q "Connected"
}

connect_to_mullvad() {
    mullvad connect
}

disconnect_from_mullvad() {
    mullvad disconnect
}

# Check if a network UUID is in the whitelist
is_whitelisted() {
    local network_uuid=$1
    for uuid in "${whitelist[@]}"; do
        [[ "$uuid" == "$network_uuid" ]] && return 0
    done
    return 1
}

# Function to check active connections and act accordingly
check_connections() {
    local active_connections
    active_connections=$(nmcli -t -f NAME,UUID con show --active | grep -v "mullvad")

    if [[ -n "$active_connections" ]]; then
        # Extract UUID from the active connection
        local network_uuid
        network_uuid=$(echo "$active_connections" | awk -F: '{print $2}')

        if is_whitelisted "$network_uuid"; then
            echo "Network $network_uuid is whitelisted. Disconnecting from Mullvad VPN."
            disconnect_from_mullvad
        else
            echo "Network $network_uuid is not whitelisted. Connecting to Mullvad VPN..."
            connect_to_mullvad
        fi
    else
        echo "No active connections. Disconnecting from Mullvad VPN."
        disconnect_from_mullvad
    fi
}

# Execute main function and redirect output to stderr
check_connections >&2

# Loop
nmcli monitor | while read -r event; do
    if [[ "$event" == *"changed"* || "$event" == *"There's no primary connection"* ]]; then
        echo "Network interface changed. Checking connections..." >&2
        check_connections >&2
    fi
done