# By connecting to the Radarr and Jellyfin API (you'll need to download the Playback Reporting plugin), this script allows you to list the least viewed or never seen films on your Jellyfin server in order to prioritise them if you ever need to make space on your storage device.

import requests
from datetime import datetime, timedelta, timezone
from dateutil import parser

RADARR_HOST = "radarr.host"
RADARR_API_KEY = "YourRadarrApiKey"

JELLYFIN_HOST = "jellyfin.host
JELLYFIN_TOKEN = "YourJellyfinToken"

radarr_url = 'https://{RADARR_HOST}/api/v3/movie?apikey={RADARR_API_KEY}'
radarr_headers = {
    'Content-Type': 'application/json',
}

report_url = 'https://{JELLYFIN_HOST}/emby/user_usage_stats/MoviesReport?days=365'
report_headers = {
    'Authorization': 'MediaBrowser Token={JELLYFIN_TOKEN}',
    'Content-Type': 'application/json',
}

whitelist_file_path = 'whitelist.txt'

with open(whitelist_file_path, 'r') as file:
    whitelist = [line.strip() for line in file]

response_radarr = requests.get(radarr_url, headers=radarr_headers)
response_report = requests.get(report_url, headers=report_headers)

if response_radarr.status_code == 200:
    response_radarr_json = response_radarr.json()

else:
    print(f"Error: {response_radarr.status_code} - {response_radarr.text}")

if response_report.status_code == 200:
    response_report_json = response_report.json()

else:
    print(f"Error: {response_report.status_code} - {response_report.text}")

movies = []

# Current date in UTC
current_date = datetime.utcnow().replace(tzinfo=timezone.utc)

for item in response_radarr_json:
    title = item["title"]
    release_date_str = item.get("digitalRelease", "2999-08-21T20:31:09Z")
    added_date_str = item["added"]
    size_on_disk = item["sizeOnDisk"]
    path = item["path"]
    target_movie = next((movie for movie in response_report_json if movie['label'] == title), None)
    play_count = target_movie['count'] if target_movie else 0

    # Convert the added_date_str to a datetime object
    added_date = datetime.fromisoformat(added_date_str[:-1])  # Remove the trailing 'Z' and convert to datetime

    release_date = datetime.fromisoformat(release_date_str[:-1]) 

    if (
        size_on_disk > 100 and
        "Hestia" not in path and
        (release_date := parser.parse(release_date_str)) > current_date + timedelta(days=30) and title not in whitelist
    ):
        movie_details = {
            'title': title,
            'added_date': added_date.date(),
            'size_on_disk': size_on_disk,
            'play_count': play_count
        }

        movies.append(movie_details)

# Custom sorting key function
def custom_sort_key(movie):
    return (movie['play_count'], movie['added_date'], movie['size_on_disk'])

# Sort movies based on the custom key
sorted_movies = sorted(movies, key=custom_sort_key, reverse=True)

# Print the sorted movies
for movie in sorted_movies:
    print(f"Title: {movie['title']}, Added Date: {movie['added_date']}, Size on Disk: {movie['size_on_disk']}, Play Count: {movie['play_count']}")
