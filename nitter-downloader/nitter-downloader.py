from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import csv
from datetime import datetime, timedelta

def format_date(date_str):
    try:
        parsed_date = datetime.strptime(date_str, "%b %d, %Y · %I:%M %p %Z")
        return parsed_date
    except ValueError:
        return None

def get_tweets(username, max_tweets=None, max_days=None):
    options = webdriver.ChromeOptions()
    # options.add_argument('--headless')
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    options.binary_location = "/usr/bin/chromium"  # Specify the path to your Chromium binary

    # Use the manually downloaded chromedriver
    service = Service("/home/user/Downloads/chromedriver-linux64/chromedriver")
    driver = webdriver.Chrome(service=service, options=options)

    base_url = f"https://nitter.poast.org/{username}"
    tweets = []
    page_counter = 0
    retries = 3

    driver.get(base_url)

    end_date = datetime.now() - timedelta(days=max_days) if max_days else None
    print(f"End date: {end_date}")

    try:
        while True:
            print(f"Scraping page {page_counter + 1}")
            WebDriverWait(driver, 10).until(
                EC.presence_of_all_elements_located((By.CSS_SELECTOR, "div.timeline-item"))
            )

            tweet_divs = driver.find_elements(By.CSS_SELECTOR, "div.timeline-item")
            for tweet_div in tweet_divs:
                try:
                    tweet_text = tweet_div.find_element(By.CSS_SELECTOR, "div.tweet-content").text.strip()
                    tweet_date_element = tweet_div.find_element(By.CSS_SELECTOR, "span.tweet-date a")
                    tweet_time = tweet_date_element.get_attribute("title").strip()
                    tweet_url = tweet_div.find_element(By.CSS_SELECTOR, "a.tweet-link").get_attribute("href").replace("nitter.poast.org", "x.com")
                    
                    stats = tweet_div.find_elements(By.CSS_SELECTOR, "span.tweet-stat")
                    comments = stats[0].text if len(stats) > 0 else "0"
                    retweets = stats[1].text if len(stats) > 1 else "0"
                    quotes = stats[2].text if len(stats) > 2 else "0"
                    likes = stats[3].text if len(stats) > 3 else "0"
                    
                    formatted_time = format_date(tweet_time)
                    if formatted_time is None:
                        print(f"Skipping tweet due to invalid date format: {tweet_time}")
                        continue

                    tweets.append({
                        'time': formatted_time.strftime("%Y-%m-%d"),
                        'text': tweet_text,
                        'url': tweet_url,
                        'comments': comments.replace(',', ''),
                        'retweets': retweets.replace(',', ''),
                        'quotes': quotes.replace(',', ''),
                        'likes': likes.replace(',', '')
                    })

                    if max_tweets and len(tweets) >= max_tweets:
                        print(f"Reached the tweet limit of {max_tweets}.")
                        return tweets
                except Exception as e:
                    print(f"Error scraping tweet: {e}")
                    continue

            last_tweet_date = format_date(tweet_divs[-1].find_element(By.CSS_SELECTOR, "span.tweet-date a").get_attribute("title").strip())
            if end_date and last_tweet_date and last_tweet_date < end_date:
                print(f"Reached the date limit. Last Tweet date: {last_tweet_date}, End date: {end_date}")
                break

            print(f"Page {page_counter + 1} scraped successfully.")
            page_counter += 1

            retry_attempts = 0
            while retry_attempts < retries:
                try:
                    show_more_buttons = driver.find_elements(By.CSS_SELECTOR, "div.show-more > a")
                    if page_counter == 1:  # First page
                        if len(show_more_buttons) == 0:
                            return tweets
                        load_more = show_more_buttons[0]
                    else:  # Subsequent pages
                        if len(show_more_buttons) < 2:
                            print(f"Not enough 'show more' buttons. Retry attempt {retry_attempts + 1}.")
                            retry_attempts += 1
                            driver.refresh()
                            time.sleep(2)
                            continue
                        load_more = show_more_buttons[1]

                    load_more.click()
                    time.sleep(2)  # Adding delay to ensure content is loaded
                    break  # Exit retry loop if click is successful
                except Exception as e:
                    print(f"Error clicking 'show more': {e}")
                    retry_attempts += 1
                    driver.refresh()
                    time.sleep(10)
                    if retry_attempts >= retries:
                        print("No more pages or error clicking 'show more'. Stopping.")
                        return tweets

    except Exception as e:
        print(f"An error occurred: {e}")
    finally:
        print("Script finished. The browser will remain open for debugging.")
        input("Press Enter to close the browser...")
        driver.quit()
    return tweets

def save_tweets_to_csv(tweets, username):
    filename = f"{username}_tweets.csv"
    with open(filename, 'w', encoding='utf-8', newline='') as file:
        writer = csv.DictWriter(file, fieldnames=['time', 'text', 'url', 'comments', 'retweets', 'quotes', 'likes'])
        writer.writeheader()
        for tweet in tweets:
            writer.writerow(tweet)
    print(f"Saved {len(tweets)} tweets to {filename}")

if __name__ == "__main__":
    username = input("Enter the Twitter username: ")
    max_tweets = input("Enter the number of tweets to scrape (leave blank if not applicable): ")
    max_days = input("Enter the number of days to scrape (leave blank if not applicable): ")

    max_tweets = int(max_tweets) if max_tweets else None
    max_days = int(max_days) if max_days else None

    if not max_tweets and not max_days:
        print("You must specify either a number of tweets or a number of days.")
    else:
        tweets = get_tweets(username, max_tweets=max_tweets, max_days=max_days)
        save_tweets_to_csv(tweets, username)
        print(f"Saved {len(tweets)} tweets to {username}_tweets.csv")
